import controllers.SantotoBankController;
import utiliy.Keyboard;

public class Main {

    public static Keyboard keyboard = new Keyboard();
    public static final SantotoBankController santotoBankController = new SantotoBankController();
    public static final String SUCCESS = "¡Transferencia exitosa!";

    public static void main(String[] args) {
        Main main = new Main();
        main.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> main.atenderCliente();
                case 2 -> main.mostrarColaClientes();
                case 3 -> main.mostrarClientes();
                case 4 -> main.imprimirTodasLasCuentas();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔═══════════════════════════════════════════════════════╗");
        System.out.println("╠----------------------Santoto Bank---------------------╣");
        System.out.println("║═══════════════════════════════════════════════════════║");
        System.out.println("║   1. Atender cliente                                  ║");
        System.out.println("║   2. Mostrar cola de clientes                         ║");
        System.out.println("║   3. Mostrar clientes                                 ║");
        System.out.println("║   4. Imprimir todas las cuentas del banco             ║");
        System.out.println("║═══════════════════════════════════════════════════════║");
        System.out.println("║   0. Salir                                            ║");
        System.out.println("╚═══════════════════════════════════════════════════════╝");
    }

    public void menuAcciones() {
        System.out.println("╔═══════════════════════════════════════════════════════╗");
        System.out.println("║   1. Retirar dinero                                   ║");
        System.out.println("║   2. Cambiar clave                                    ║");
        System.out.println("║   3. Ver saldo                                        ║");
        System.out.println("╚═══════════════════════════════════════════════════════╝");
    }

    public void atenderCliente() {
        // La cola de clientes se genera de forma aleatoria
        santotoBankController.agregarClienteActual();
        int numeroCuenta;
        String clave;
        do {
            System.out.print("  Buenos días señ@r " + santotoBankController.getClienteActual().getNombre());
            santotoBankController.imprimirCuentasClienteActual();
            numeroCuenta = keyboard.readValidPositiveInteger("  Ingrese su número de cuenta: ");
            System.out.print("  Ingrese la clave de la cuenta: ");
            clave = keyboard.readLine();
        } while (!santotoBankController.validarInfoCuenta(numeroCuenta, clave));
        realizarAcciones();
    }

    public void realizarAcciones() {
        menuAcciones();
        System.out.println(" Ingrese las acciones que va a realizar: ");
        santotoBankController.agregarAcciones(keyboard.readValidActions("  Ingrese la primera acción: "));
        resolverAcciones();
        santotoBankController.agregarAcciones(keyboard.readValidActions("  Ingrese la segunda acción: "));
        resolverAcciones();
        santotoBankController.agregarAcciones(keyboard.readValidActions("  Ingrese la tercera acción: "));
        resolverAcciones();

        santotoBankController.atenderCliente();
    }

    public void resolverAcciones() {
        switch (santotoBankController.getClienteActual().getAcciones().peek()) {
            case 1 -> retirarDinero();
            case 2 -> cambiarClave();
            default -> verSaldo();
        }
        santotoBankController.getClienteActual().getAcciones().pop();
    }

    public void retirarDinero() {
        if (santotoBankController.retirarDinero(keyboard.readValidPositiveDouble("  ¿Cuánto dinero desea retirar? "))) {
            System.out.println("\t" + SUCCESS + "\n");
        } else {
            System.out.println("  ¡Saldo insuficiente!\n");
        }
    }

    public void cambiarClave() {
        System.out.print("  Ingrese la nueva clave: ");
        santotoBankController.cambiarClave(keyboard.readLine());
        System.out.println("\t" + SUCCESS);
    }

    public void verSaldo() {
        System.out.println("  El saldo de la cuenta N° " + santotoBankController.getCuentaActual().getNumeroCuenta() +
                " es $" + santotoBankController.obtenerSaldo() + "\n");
    }

    public void mostrarColaClientes() {
        santotoBankController.imprimirColaDeClientes();
    }

    public void mostrarClientes() {
        santotoBankController.imprimirClientes();
    }

    public void imprimirTodasLasCuentas(){
        santotoBankController.imprimirTodasLasCuentas();
    }

}
