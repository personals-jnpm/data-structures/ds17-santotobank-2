package models;

import java.util.Stack;

public class Cliente {

    private int idCliente;
    private String nombre;
    private String cedula;
    private Stack<Integer> acciones;

    public Cliente() {
        acciones = new Stack<>();
    }

    public Cliente(int idCliente, String nombre, String cedula, Stack<Integer> acciones) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.cedula = cedula;
        this.acciones = acciones;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Stack<Integer> getAcciones() {
        return acciones;
    }

    public void setAcciones(Stack<Integer> acciones) {
        this.acciones = acciones;
    }

    @Override
    public String toString() {
        return "Cliente {\n" +
                " \t id: " + idCliente +
                ", nombre: " + nombre +
                ", cedula: " + cedula +
                ", \n \t acciones: " + acciones +
                "}\n";
    }

    public void agregarAccion(int element) {
        acciones.add(element);
    }

}
