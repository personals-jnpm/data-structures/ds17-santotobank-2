package models;

public class Cuenta {

    private int numeroCuenta;
    private String clave;
    private double saldo;

    public Cuenta() {
    }

    public Cuenta(int numeroCuenta, String clave, double saldo) {
        this.numeroCuenta = numeroCuenta;
        this.clave = clave;
        this.saldo = saldo;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "numeroCuenta: " + numeroCuenta +
                ", clave: " + clave +
                ", saldo: " + saldo;
    }
}
