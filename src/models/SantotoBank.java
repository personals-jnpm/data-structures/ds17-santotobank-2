package models;

import java.util.*;

public class SantotoBank {

    private List<Cliente> clientes;
    private Queue<Cliente> colaClientes;
    private HashMap<String, Cuenta> cuentas;

    public SantotoBank() {
        clientes = new ArrayList<>();
        colaClientes = new LinkedList<>();
        cuentas = new HashMap<>();

        initClientes();
        initCuentas();
        generateRandomOrder();
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Queue<Cliente> getColaClientes() {
        return colaClientes;
    }

    public void setColaClientes(Queue<Cliente> colaClientes) {
        this.colaClientes = colaClientes;
    }

    public HashMap<String, Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(HashMap<String, Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public void agregarCuenta(Cuenta cuenta, int idCliente) {
        cuentas.put("" + cuenta.getNumeroCuenta() + "-" + idCliente, cuenta);
    }

    public void actualizarCuenta(int idCliente, Cuenta cuenta) {
        cuentas.replace("" + cuenta.getNumeroCuenta() + "-" + idCliente, cuenta);
        System.out.println(cuentas.get("" + cuenta.getNumeroCuenta() + "-" + idCliente));
    }

    public void validarInfoCuenta(int idCliente, int numeroCuenta, String clave) throws Exception {
        try {
            Cuenta cuenta = cuentas.get("" + numeroCuenta + "-" + idCliente);
            if (!cuenta.getClave().equals(clave)) {
                throw new Exception("  ¡Clave incorrecta!");
            }
        } catch (NullPointerException e) {
            throw new Exception("  ¡Número de cuenta incorrecto!");
        }
    }

    public Cuenta obtenerCuenta(int numeroCuenta) {
        for (Cuenta cuenta : cuentas.values()) {
            if (cuenta.getNumeroCuenta() == numeroCuenta) {
                return cuenta;
            }
        }
        return null;
    }

    public void imprimirCuentas(int idCliente) {
        System.out.println("\n \tCuentas de " + obtenerClienteActual().getNombre() + " [");
        for (Map.Entry<String, Cuenta> cuenta : cuentas.entrySet()) {
            if (cuenta.getKey().substring(getSeparator(cuenta.getKey())).equals("" + idCliente)) {
                System.out.println("\t \t " + cuenta.getValue().toString());
            }
        }
        System.out.println("\t]");
    }

    private int getSeparator(String key) {
        for (int i = 0; i < key.length(); i++) {
            if (key.charAt(i) == '-') {
                return i + 1;
            }
        }
        return 0;
    }

    public void imprimirTodasLasCuentas(){
        for (Map.Entry<String, Cuenta> cuenta : cuentas.entrySet()) {
                System.out.println("\t \t " + "Key [" + cuenta.getKey()+ "] -> {" +
                        cuenta.getValue().toString() + "}");
        }
        System.out.println("\t]");
    }

    // Métodos de clientes
    public void atenderCliente() {
        colaClientes.poll();
    }

    public Cliente obtenerClienteActual() {
        return colaClientes.peek();
    }

    public void imprimirClientes() {
        System.out.println("Clientes del banco [");
        for (Cliente cliente : clientes) {
            System.out.println("\t" + cliente.toString());
        }
        System.out.print("]\n");
    }

    public void imprimirColaClientes() {
        System.out.println("Clientes en cola [");
        for (Cliente cliente : colaClientes) {
            System.out.println("\t" + cliente.toString());
        }
        System.out.print("]\n");
    }

    private void generateRandomOrder() {
        List<Integer> listaTemporal = new ArrayList<>();
        for (int i = 0; i < clientes.size() - 1; i++) {
            int idCliente = (int) (Math.random() * (10));
            if (i == 0) {
                listaTemporal.add(idCliente);
                colaClientes.add(buscarCliente(idCliente));
            } else if (!listaTemporal.contains(idCliente)) {
                listaTemporal.add(idCliente);
                colaClientes.add(buscarCliente(idCliente));
            }
        }
    }

    public Cliente buscarCliente(int id) {
        for (Cliente cliente : clientes) {
            if (cliente.getIdCliente() == id) {
                return cliente;
            }
        }
        return null;
    }

    private void initClientes() {
        clientes.add(new Cliente(0, "Carlos", "1254123654", new Stack<>()));
        clientes.add(new Cliente(1, "María", "5423687", new Stack<>()));
        clientes.add(new Cliente(2, "Diana", "45678214", new Stack<>()));
        clientes.add(new Cliente(3, "Oscar", "1021458793", new Stack<>()));
        clientes.add(new Cliente(4, "Mariana", "4563217893", new Stack<>()));
        clientes.add(new Cliente(5, "Catalina", "65478963", new Stack<>()));
        clientes.add(new Cliente(6, "Fernando", "1324563218", new Stack<>()));
        clientes.add(new Cliente(7, "Andrés", "4521369", new Stack<>()));
        clientes.add(new Cliente(8, "Nicolás", "3256348521", new Stack<>()));
        clientes.add(new Cliente(9, "Laura", "7894521362", new Stack<>()));
    }

    private void initCuentas() {
        cuentas.put("4528-0", new Cuenta(4528, "ki78", 50000));
        cuentas.put("4578-0", new Cuenta(4578, "qe53", 70000));
        cuentas.put("3578-0", new Cuenta(3578, "br32", 59000));

        cuentas.put("4235-1", new Cuenta(4235, "er48", 83000));
        cuentas.put("2356-1", new Cuenta(2356, "nj30", 49000));
        cuentas.put("3698-1", new Cuenta(3698, "er78", 100000));

        cuentas.put("1236-2", new Cuenta(1236, "uy41", 63000));
        cuentas.put("1000-2", new Cuenta(1000, "re20", 78000));
        cuentas.put("7842-2", new Cuenta(7842, "bt12", 52500));

        cuentas.put("4362-3", new Cuenta(4362, "ki13", 40000));
        cuentas.put("7823-3", new Cuenta(7823, "qw78", 37000));
        cuentas.put("2039-3", new Cuenta(2039, "cx78", 23000));

        cuentas.put("4125-4", new Cuenta(4125, "we45", 41000));
        cuentas.put("1368-4", new Cuenta(1368, "nj48", 89000));
        cuentas.put("7426-4", new Cuenta(7426, "gb12", 75000));

        cuentas.put("7265-5", new Cuenta(7265, "fr98", 15000));
        cuentas.put("1235-5", new Cuenta(1235, "yu28", 64000));
        cuentas.put("7895-5", new Cuenta(7895, "wb78", 56300));

        cuentas.put("4265-6", new Cuenta(4265, "ff00", 81000));
        cuentas.put("3695-6", new Cuenta(3695, "er32", 87000));
        cuentas.put("7532-6", new Cuenta(7532, "oi36", 45000));

        cuentas.put("9634-7", new Cuenta(9634, "ñl16", 59000));
        cuentas.put("1201-7", new Cuenta(1201, "an53", 49000));
        cuentas.put("4126-7", new Cuenta(4126, "rt00", 73000));

        cuentas.put("7436-8", new Cuenta(7436, "wn46", 85000));
        cuentas.put("7126-8", new Cuenta(7126, "yu10", 150000));

        cuentas.put("1039-9", new Cuenta(1039, "qc89", 96000));
        cuentas.put("6530-9", new Cuenta(6530, "ja30", 300000));
    }
}
