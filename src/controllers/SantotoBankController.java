package controllers;

import models.Cliente;
import models.Cuenta;
import models.SantotoBank;

public class SantotoBankController {

    private final SantotoBank santotoBank;
    private Cliente clienteActual;
    private Cuenta cuentaActual;

    public SantotoBankController() {
        clienteActual = new Cliente();
        santotoBank = new SantotoBank();
    }

    public Cliente getClienteActual() {
        return clienteActual;
    }

    public void setClienteActual(Cliente clienteActual) {
        this.clienteActual = clienteActual;
    }

    public Cuenta getCuentaActual() {
        return cuentaActual;
    }

    public void setCuentaActual(Cuenta cuentaActual) {
        this.cuentaActual = cuentaActual;
    }

    public void agregarClienteActual() {
        clienteActual = santotoBank.obtenerClienteActual();
    }

    public void atenderCliente() {
        santotoBank.actualizarCuenta(clienteActual.getIdCliente(), cuentaActual);
        santotoBank.atenderCliente();
    }

    public void agregarAcciones(int element) {
        clienteActual.agregarAccion(element);
    }

    public boolean validarInfoCuenta(int numeroCuenta, String clave) {
        try {
            santotoBank.validarInfoCuenta(clienteActual.getIdCliente(), numeroCuenta, clave);
            cuentaActual = santotoBank.obtenerCuenta(numeroCuenta);
            System.out.println("\n  ¡Credenciales correctas!\n");
            return true;
        } catch (Exception e) {
            System.out.println("\n" + e.getMessage() + "\n");
            return false;
        }
    }

    public boolean retirarDinero(double monto) {
        if (cuentaActual.getSaldo() > monto){
            cuentaActual.setSaldo(cuentaActual.getSaldo() - monto);
            return true;
        }
        return false;
    }

    public void cambiarClave(String nuevaClave) {
        cuentaActual.setClave(nuevaClave);
    }

    public double obtenerSaldo() {
        return cuentaActual.getSaldo();
    }

    public void imprimirCuentasClienteActual() {
        santotoBank.imprimirCuentas(clienteActual.getIdCliente());
    }

    public void imprimirClientes() {
        santotoBank.imprimirClientes();
    }

    public void imprimirColaDeClientes() {
        santotoBank.imprimirColaClientes();
    }

    public void imprimirTodasLasCuentas(){
        santotoBank.imprimirTodasLasCuentas();
    }

}
